﻿// Decompiled with JetBrains decompiler
// Type: InvokeInstructionApp.OrgCredentialsClass
// Assembly: InvokeInstructionApp, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 0250C811-D1D8-4039-832B-93440B4DB6CF
// Assembly location: D:\work\source\repo\SP_prophet_invoke-instructions\InvokeInstructionApp-app\InvokeInstructionApp.exe

namespace InvokeInstructionApp
{
  internal class OrgCredentialsClass
  {
    public string Name { get; set; }

    public string Username { get; set; }

    public string Password { get; set; }

    public string SecurityToken { get; set; }

    public OrgCredentialsClass(string aName, string aUser, string sPass, string aTok)
    {
      this.Name = aName;
      this.Username = aUser;
      this.Password = sPass;
      this.SecurityToken = aTok;
    }
  }
}
