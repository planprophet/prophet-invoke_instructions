﻿// Decompiled with JetBrains decompiler
// Type: InvokeInstructionApp.SFApiRestHelper
// Assembly: InvokeInstructionApp, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 0250C811-D1D8-4039-832B-93440B4DB6CF
// Assembly location: D:\work\source\repo\SP_prophet_invoke-instructions\InvokeInstructionApp-app\InvokeInstructionApp.exe

using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace InvokeInstructionApp
{
  public static class SFApiRestHelper
  {
    public static async Task<string> RefreshAuthenticateTokenAsync(
      string SfConsumerKey,
      string SfConsumerSecret,
      string SfUserName,
      string SfPass,
      string SfToken,
      string SfLoginUrl,
      string parameters)
    {
      HttpClient authClient = new HttpClient();
      HttpContent content = (HttpContent) new FormUrlEncodedContent((IEnumerable<KeyValuePair<string, string>>) new Dictionary<string, string>()
      {
        {
          "grant_type",
          "password"
        },
        {
          "client_id",
          SfConsumerKey ?? ""
        },
        {
          "client_secret",
          SfConsumerSecret ?? ""
        },
        {
          "username",
          SfUserName ?? ""
        },
        {
          "password",
          SfPass + SfToken
        }
      });
      string siteURL = SfLoginUrl ?? "login";
      try
      {
        HttpResponseMessage message = authClient.PostAsync(string.Format("https://{0}.salesforce.com/services/oauth2/token", (object) siteURL), content).Result;
        if (!message.IsSuccessStatusCode)
          return string.Format("Failed {0} Status Code", (object) message.StatusCode);
        string responseString = await message.Content.ReadAsStringAsync();
        JObject obj = JObject.Parse(responseString);
        string oauthToken = (string) obj["access_token"];
        string instance_url = (string) obj["instance_url"];
        return string.Format("Succes {0} Status Code {1}", (object) message.StatusCode, (object) SFApiRestHelper.EndpointResultAsync(HttpMethod.Post, instance_url, "/services/apexrest/" + Properties.Settings.Default.nspace + "/instruction", oauthToken, new StringContent(parameters)).Result);
      }
      catch (Exception ex)
      {
        return string.Format("Exception: {0}", (object) ex.Message);
      }
    }

    public static async Task<string> EndpointResultAsync(
      HttpMethod httpMethod,
      string serviceUrl,
      string restQuery,
      string oauthToken,
      StringContent content)
    {
      HttpClient queryClient = new HttpClient();
      string result = "";
      HttpRequestMessage request = new HttpRequestMessage(httpMethod, serviceUrl + restQuery);
      request.Headers.Add("Authorization", "Bearer " + oauthToken);
      if (content != null && httpMethod != HttpMethod.Get)
        request.Content = (HttpContent) content;
      HttpResponseMessage response = await queryClient.SendAsync(request);
      result = await response.Content.ReadAsStringAsync();
      return result;
    }
  }
}
