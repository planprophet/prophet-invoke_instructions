﻿// Decompiled with JetBrains decompiler
// Type: InvokeInstructionApp.Program
// Assembly: InvokeInstructionApp, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 0250C811-D1D8-4039-832B-93440B4DB6CF
// Assembly location: D:\work\source\repo\SP_prophet_invoke-instructions\InvokeInstructionApp-app\InvokeInstructionApp.exe

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace InvokeInstructionApp
{
    internal class Program
    {
        private const string CANCEL_MESSAGE = "Process canceled by user";

        private static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("PProphet Post Install Run Instruction Tool");
                Console.WriteLine("Organizations included in File data.txt will be posted to run post install script. ");
                Console.WriteLine("Make sure credencials included in File data.txt are right.");
                Console.WriteLine("---------------------------------------------------- ");
                Console.WriteLine("Install script option: ");
                Console.WriteLine("1 - Run only post install script.");
                Console.WriteLine("2 - Create Default Communications.");
                Console.WriteLine("3 - Generate build.properties for ANT deployment.");
                /*
                Console.WriteLine("4 - Create default Email templates Folders.");
                Console.WriteLine("5 - Create default Email templates.");
                */
                Console.WriteLine("Select option.");
                string upper = Console.ReadLine().Trim().ToUpper();
                string parameters = string.Empty;
                if (upper == "2")
                    parameters = "{\"deployDefaultCommunications\":true,\"deployDefaultEmailTemplateFolders\":false,\"deployDefaultEmailTemplates\":false}";
                else if (upper == "4")
                    parameters = "{\"deployDefaultEmailTemplateFolders\":true}";
                else if (upper == "5")
                    parameters = "{\"deployDefaultEmailTemplates\":true}";

                Console.WriteLine("---------------------------------------------------- ");
                List<OrgCredentialsClass> credentialsClassList = JsonConvert.DeserializeObject<List<OrgCredentialsClass>>(string.Join("", File.ReadAllLines("data.txt")));
                Console.WriteLine(string.Format("Following {0} organizations are about to be processed.", (object)credentialsClassList.Count));
                foreach (OrgCredentialsClass credentialsClass in credentialsClassList)
                    Console.WriteLine(string.Format("{0}", (object)credentialsClass.Name));
                Program.AskProceed();
                if (upper == "3")
                    GenerateBuildProperties(credentialsClassList);
                else
                    foreach (OrgCredentialsClass credentialsClass in credentialsClassList)
                    {
                        Console.WriteLine(string.Format("{0} - {1} - Sending Post For Instruction", (object)credentialsClass.Name, (object)credentialsClass.Username));
                        Console.WriteLine(SFApiRestHelper.RefreshAuthenticateTokenAsync("3MVG9KsVczVNcM8wHIqX9OtZuM2uvm5A12zZewO1MGCo0bUESjJdkq7nUNx7iWm5.6AkdoDFLj3eno90UV3LR", "8264081545415329552", credentialsClass.Username, credentialsClass.Password, credentialsClass.SecurityToken, "login", parameters).Result);
                        Console.WriteLine();
                    }
                Console.WriteLine("Process completed ");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message != "Process canceled by user" ? string.Format("Exception Ocurred :{0}", (object)ex.ToString()) : ex.Message);
            }
            finally
            {
                Console.ReadLine();
            }
        }

        private static void GenerateBuildProperties(List<OrgCredentialsClass> credentialsClassList)
        {
            /*

#FSI55371 - Sir Speedy Miami Lakes 

sf.password.planprophetester.miami = myskydev02QuO4EZiU6El4mf6hVm5CUVQt             
             */
            var text = new List<String>();
            var siteName = 0;
            foreach (OrgCredentialsClass credentialsClass in credentialsClassList)
            {
                siteName++;
                text.Add($"#{credentialsClass.Name}");
                text.Add($"sf.username.planprophet.p{siteName} = {credentialsClass.Username}");
                text.Add($"sf.password.planprophet.p{siteName} = {credentialsClass.Password}{credentialsClass.SecurityToken}");
                text.Add($"");
                Console.WriteLine();
            }

            text.Add(" \n # Prod server URL \n sf.serverurl.login = https://login.salesforce.com");

            File.WriteAllText("build.properties", String.Join(Environment.NewLine, text));
        }

        private static void AskProceed()
        {
            Console.WriteLine("Press (Y) to proceed.");
            if (Console.ReadLine().Trim().ToUpper() != "Y")
                throw new Exception("Process canceled by user");
        }

        private static void CreateOrgsJSON() => File.WriteAllText("data.txt", JsonConvert.SerializeObject((object)new List<OrgCredentialsClass>()
    {
      new OrgCredentialsClass("DEV", "planprophet-ppqa002@planprophet.com", "myskydev01", "VkspvVceyr47DiTh7LxC6Gi14"),
      new OrgCredentialsClass("Sirpeedy Miami", "planprophet-fsi55371@planprophet.com", "myskydev01", "QuO4EZiU6El4mf6hVm5CUVQt"),
      new OrgCredentialsClass("AFB12121600 - Allegra Hampton Roads", "planprophet-afb12121600@planprophet.com", "myskydev02", "VvWGImqGe3tCTBubvYfeYuPyf"),
      new OrgCredentialsClass("AFB358 - Allegra Naples", "planprophet-afb358@planprophet.com", "myskydev02", "O8iiQu8Y8jPDLNzZn3gbdsNn "),
      new OrgCredentialsClass("FSI50270 - Sir Speedy Orlando", "planprophet-fsi50270@planprophet.com", "myskydev02", "UJHvpyqA2VbYS6wyKR4fneeY "),
      new OrgCredentialsClass(" PPUAT001 - PlanProphet UAT", "planprophet-ppuat001@planprophet.com", "myskydev02", "Q7mk2zmBkT7iROCGkZMtbaSrL"),
      new OrgCredentialsClass("PPAFBCORP002 - Allegra Corporate", "planprophet-ppafbcorp002@planprophet.com", "myskydev01", "xTxQTxavd6XOAmKghAroREWB"),
      new OrgCredentialsClass("FSI06507- Sir Speedy Green Bay ", "planprophet-fsi06507@planprophet.com", "myskydev02", "yEDY4ks5nWfQCTWk51ZO18uEV"),
      new OrgCredentialsClass("FSI07099- Sir Speedy Printing 7099", "planprophet-fsi07099@planprophet.com", "myskydev02", "QFc6aFkC6ozX3nd2Job3NFM9Q"),
      new OrgCredentialsClass("FSI301280 - Sir Speedy Music City ", "planprophet-fsi301280@planprophet.com", "myskydev02", "I4CtFASg3ejM9hmDdtBZWZwo3"),
      new OrgCredentialsClass("PIP4591316- PIP Printing Peoria ", "planprophet-pip4591316@planprophet.com", "myskydev02", "GIgvcIUq8UkUxBGqzXraZmrkP"),
      new OrgCredentialsClass("AFB07098500 - Allegra Pompano Beach  ", "planprophet-afb07098500@planprophet.com", "myskydev02", "aEEdR8Ju8kJZ8HKSSimK4DP2"),
      new OrgCredentialsClass("IND1001 - EveryDay Envelope", "planprophet-ind1001@planprophet.com", "myskydev02", "aGqUWY4dnahSMIzb7gkAQtW7"),
      new OrgCredentialsClass("AFB1284 - Allegra Rocky Mount", "planprophet-afb1284@planprophet.com", "myskydev01", "edtmFfFX538RYh6CdJdF8kKB"),
      new OrgCredentialsClass("FSI61060 - Sir Speedy Naperville ", "planprophet-fsi61060@planprophet.com", "myskydev02", "Y9biJQ8p70QcRjx3eYloeQN8"),
      new OrgCredentialsClass("AFB1202 - Allegra Little Rock  ", "planprophet-afb1202@planprophet.com", "myskydev01", "hTaWYyJTsHRwLWSa5Iy9kl3fF"),
      new OrgCredentialsClass("AFB1023901 - Allegra Grand Rapids", "planprophet-afb1023901@planprophet.com", "myskydev01", "DZ4ZoJwJeGjSTEJgIYDRb87Dw"),
      new OrgCredentialsClass("AFB07032402 - Allegra Tampa", "planprophet-afb07032402@planprophet.com", "myskydev01", "xKRedLUv4mUaOPFpgM9MZPPvH"),
      new OrgCredentialsClass("AFB52001003 - Allegra Hamilton", "planprophet-afb52001003@planprophet.com", "myskydev01", "SG2EAyr6tJDSbtW4tLxYMZg4"),
      new OrgCredentialsClass("AFB43071600 - Allegra Reno", "planprophet-afb43071600@planprophet.com", "myskydev01", "ICYQThkBANSw5M3ZwXb0v7iv"),
      new OrgCredentialsClass("AFB52000801 - Allegra Windsor West  ", "planprophet-afb52000801@planprophet.com", "myskydev01", "OxFWNO30U5AomhhngV2Ts728y"),
      new OrgCredentialsClass("IND1002 - TriState Graphics ", "planprophet-ind1002@planprophet.com", "myskydev01", "x9n8X0xPTz5MZWXt480jJqf8")
    }));
    }
}

